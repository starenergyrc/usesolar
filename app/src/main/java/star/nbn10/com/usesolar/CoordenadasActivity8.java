package star.nbn10.com.usesolar;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.jar.Manifest;

public class CoordenadasActivity8 extends AppCompatActivity {

    private TextView txtLatitude;
    private TextView txtLongitude;
    private TextView txtEndere;
    private TextView txtCEP;
    private TextView txtCidade;
    private TextView txtEstado;
    private TextView txtPais;

    private Location location;
    private LocationManager locationManager;

    private Address endereco;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coordenadas8);

        txtLatitude =(TextView) findViewById(R.id.txtLatitude);
        txtLongitude =(TextView) findViewById(R.id.txtLongitude);
        txtEndere =(TextView) findViewById(R.id.txtEndere);
        txtCEP =(TextView) findViewById(R.id.txtCEP);
        txtCidade =(TextView) findViewById(R.id.txtCidade);
        txtEstado =(TextView) findViewById(R.id.txtEstado);
        txtPais =(TextView) findViewById(R.id.txtPais);


        double latitude = 0.0;
        double longitude = 0.0;

        if(ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED){

        }else {
            locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        }
         if (location != null){

            longitude = location.getLongitude();
            latitude = location.getLatitude();
         }

        txtLongitude.setText(" Longitude: " + longitude);
        txtLatitude.setText(" Latitude: " + latitude);
            
        try {
            endereco = buscarEndereco(latitude, longitude);

            txtCidade.setText(" Cidade: " + endereco.getLocality());
            txtEstado.setText(" Estado: " + endereco.getAdminArea());
            txtPais.setText(" País: " + endereco.getCountryName());
            txtEndere.setText(" Telefone: " + endereco.getPhone());
            txtCEP.setText(" CEP: " + endereco.getPostalCode());

            
        } catch (IOException e){

            Log.i("GPS", e.getMessage());
        }

        }

    private Address buscarEndereco(double latitude, double longitude)
                      throws IOException{
        Geocoder geocoder;
        Address address = null;
        List<Address> addresses;

        geocoder = new Geocoder(getApplicationContext());

        addresses = geocoder.getFromLocation(latitude,longitude,1);                  // o numero 1 é a quantidade de endereços de resposta se quiser mais é so colocar 10, 100 e ele retornba 100 endereços na localizaçao
        if (addresses.size()>0){
            address = addresses.get(0);
        }
        return address;
    }
}
