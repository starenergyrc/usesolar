package star.nbn10.com.usesolar;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class GoogleMapsActivity3 extends FragmentActivity implements OnMapReadyCallback,
                               GoogleMap.OnMapClickListener{

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_maps3);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(this);

        mMap.getUiSettings().setZoomControlsEnabled(true);


        // Add a marker in Sydney and move the camera
        LatLng florianopolis = new LatLng( -27.62736079, -48.46693990 );

        MarkerOptions marker = new MarkerOptions();
        marker.position(florianopolis);
        marker.title("Star Energy RC- Florianopolis - SC - Brasil");

        mMap.addMarker(marker);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(florianopolis));      // coloca os botoes de zoom
        mMap.setMyLocationEnabled(true);                                    // coloca a localização atual bolinha azul
        mMap.setTrafficEnabled(true);                                       // mostra as condiçoes de tráfico atual
        mMap.setBuildingsEnabled(true);


    }

    @Override
    public void onMapClick(LatLng latLng) {

        Toast.makeText(getBaseContext()," Coordenadas: " + latLng.toString(),Toast.LENGTH_LONG).show();
    }
}
