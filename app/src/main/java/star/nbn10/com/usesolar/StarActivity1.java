package star.nbn10.com.usesolar;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class StarActivity1 extends AppCompatActivity {

    private Button sos;
    private Button localizacao;
    private Button altimetria;
    private Button climaetempo;
    private Button satelite;
    private Button configuracao;
    private Button coordenadas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_star1);

        sos =(Button) findViewById(R.id.botaoSOSid);
        sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StarActivity1.this,SOSActivity2.class));
            }
        });
      localizacao = (Button) findViewById(R.id.botaoGPSid);
       localizacao.setOnClickListener(new View.OnClickListener() {
           @Override
          public void onClick(View v) {
             startActivity(new Intent(StarActivity1.this,GoogleMapsActivity3.class));
          }
       });

        altimetria = (Button) findViewById(R.id.botaoAltimetriaid);
        altimetria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StarActivity1.this,AltiActivity4.class));
            }
        });

        climaetempo = (Button) findViewById(R.id.botaoClimaid);
        climaetempo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StarActivity1.this,ClimaActivity5.class));
            }
        });

        satelite = (Button) findViewById(R.id.botaoSATid);
        satelite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StarActivity1.this,SatActivity6.class));
            }
        });

        configuracao = (Button) findViewById(R.id.botaoConfigid);
        configuracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StarActivity1.this,ConfguracaoActivity7.class));
            }
        });

        coordenadas = (Button) findViewById(R.id.botaoCoordeid);
        coordenadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StarActivity1.this,CoordenadasActivity8.class));
            }
        });





    }
}

